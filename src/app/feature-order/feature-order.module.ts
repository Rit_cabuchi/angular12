import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FeatureOrderRoutes } from './feature-order.routing';
import { ReactiveFormsModule } from '@angular/forms';

/**Pages */
import { OrderComponent } from './pages/order/order.component';
import { AddOrderComponent } from './pages/add-order/add-order.component';
import { UpdateOrderComponent } from './pages/update-order/update-order.component';

/**Module */
import { CoreModule } from '@core/core.module';
import { SharedModule } from '@shared/shared.module';

@NgModule({
  declarations: [OrderComponent, AddOrderComponent, UpdateOrderComponent],
  imports: [
    CommonModule,
    FeatureOrderRoutes,
    CoreModule,
    SharedModule,
    ReactiveFormsModule,
  ],
})
export class FeatureOrderModule {}
