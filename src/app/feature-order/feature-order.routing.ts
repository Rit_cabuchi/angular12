import { Routes, RouterModule } from '@angular/router';

import { OrderComponent } from './pages/order/order.component';
import { AddOrderComponent } from './pages/add-order/add-order.component';
import { UpdateOrderComponent } from './pages/update-order/update-order.component';

const routes: Routes = [
  {
    path: '',
    component: OrderComponent,
  },
  {
    path: 'add-order',
    component: AddOrderComponent,
  },
  {
    path: 'update-order/:orderID',
    component: UpdateOrderComponent,
  },
];

export const FeatureOrderRoutes = RouterModule.forChild(routes);
