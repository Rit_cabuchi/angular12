import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PageNotFoundComponent } from '@core/components/page-not-found/page-not-found.component';
const routes: Routes = [
  {
    path: 'order',
    loadChildren: () =>
      import('./feature-order/feature-order.module').then(
        (m) => m.FeatureOrderModule
      ),
  },
  {
    path: '',
    redirectTo: 'order',
    pathMatch: 'full',
  },
  { path: '**', pathMatch: 'full', component: PageNotFoundComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
