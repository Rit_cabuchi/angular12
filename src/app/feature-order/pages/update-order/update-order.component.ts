import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { OrderService } from '@core/services/order.service';
import { Order, Ordersm } from 'data';
@Component({
  selector: 'app-update-order',
  templateUrl: './update-order.component.html',
  styleUrls: ['./update-order.component.scss'],
})
export class UpdateOrderComponent implements OnInit {
  ngOnInit() {
    const routeParams = this.route.snapshot.paramMap;
    const orderIdFromRoute = routeParams.get('orderID');
    this.id = orderIdFromRoute;
    // console.log(typeof orderIdFromRoute);
    this.orderService
      .getOrdersm(`${orderIdFromRoute}`)
      .subscribe((res: any) => {
        const { data }: { data: Ordersm } = res;
        this.order = data;
        console.log('this.o', this.order);
        this.orderForm.controls['name'].setValue(this.order?.name);
        this.orderForm.controls['quality'].setValue(this.order?.quality);
        this.orderForm.controls['price'].setValue(this.order?.price);
      });
  }
  id: any;
  order: Ordersm | undefined;
  orderForm = this.formBuilder.group({
    name: '',
    quality: '',
    price: '',
  });
  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private orderService: OrderService,
    private route: ActivatedRoute
  ) {}
  onSubmit(): void {
    if (window.confirm('Are you sure, you want to update?')) {
      this.orderService
        .updateOrder(this.id, this.orderForm.value)
        .subscribe((data) => {
          console.log('dat', data);
          this.router.navigate(['/']);
          this.orderForm.reset();
        });
    }
  }
  handleCancel() {
    this.orderForm.reset();
    this.router.navigate(['/']);
  }
}
