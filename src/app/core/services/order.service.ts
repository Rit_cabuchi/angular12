import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { retry, catchError } from 'rxjs/operators';

import { Injectable } from '@angular/core';
import { Order, orders, Ordersm } from 'data';
import { environment } from '@env';

@Injectable({
  providedIn: 'root',
})
export class OrderService {
  apiURL: string = environment.apiURL;
  orders: Order[] = orders;

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
    }),
  };

  constructor(private http: HttpClient) {}

  getOrdersm(id?: string | undefined): Observable<any> {
    let url: string = `${this.apiURL}/ordersm`;
    if (id) url = `${url}/${id}`;
    return this.http.get<any>(`${url}`);
  }

  addOrder(order: Ordersm): Observable<any> {
    return this.http
      .post<any>(`${this.apiURL}/ordersm`, order, this.httpOptions)
      .pipe(retry(1), catchError(this.handleError));
  }

  updateOrder(id: string, order: any): Observable<any> {
    return this.http
      .put<any>(`${this.apiURL}/ordersm/${id}`, order, this.httpOptions)
      .pipe(retry(1), catchError(this.handleError));
  }

  getOrders() {
    return this.orders;
  }

  deleteOrder(id: string) {
    return this.http
      .delete<any>(`${this.apiURL}/ordersm/${id}`)
      .pipe(retry(1), catchError(this.handleError));
  }

  // Error handling
  handleError(error: any) {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      // Get client-side error
      errorMessage = error.error.message;
    } else {
      // Get server-side error
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    window.alert(errorMessage);
    return throwError(errorMessage);
  }
}
