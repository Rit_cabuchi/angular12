import { Component, OnInit } from '@angular/core';
import { headerTableOrder, Order, orders, Ordersm } from 'data';
import { OrderService } from '@core/services/order.service';
import { Observable } from 'rxjs';
@Component({
  selector: 'app-order',
  templateUrl: './order.component.html',
  styleUrls: ['./order.component.scss'],
})
export class OrderComponent implements OnInit {
  headerTable: string[] = headerTableOrder;
  orders = this.orderService.getOrders();
  ordersm: any = [];
  constructor(private orderService: OrderService) {}
  handleAddOrder() {
    console.log('hi');
  }
  handleASC() {
    this.ordersm = this.ordersm.sort((a: Ordersm, b: Ordersm) => {
      return a.quality - b.quality;
    });
  }
  handleDESC() {
    this.ordersm = this.ordersm.sort((a: Ordersm, b: Ordersm) => {
      return b.quality - a.quality;
    });
  }

  ngOnInit() {
    this.loadOrders();
  }

  // Get orders list
  async loadOrders() {
    return this.orderService.getOrdersm().subscribe((res: any) => {
      const { data }: { data: Ordersm } = res;
      // this.ordersm = [...data];
      this.ordersm = data;
    });
  }
  /**Delete Order */
  handleDelete(orderID: string) {
    if (window.confirm('Are you sure, you want to delete?')) {
      this.orderService.deleteOrder(orderID).subscribe((data) => {
        console.log('data', data);
        this.loadOrders();
      });
    }
    // this.orders = this.orderService.deleteOrder(orderID);
  }
}
