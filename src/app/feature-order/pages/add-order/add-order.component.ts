import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { OrderService } from '@core/services/order.service';
@Component({
  selector: 'app-add-order',
  templateUrl: './add-order.component.html',
  styleUrls: ['./add-order.component.scss'],
})
export class AddOrderComponent {
  orderForm = this.formBuilder.group({
    name: '',
    quality: '',
    price: '',
  });
  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private orderService: OrderService
  ) {}
  onSubmit(): void {
    this.orderService.addOrder(this.orderForm.value).subscribe((data: {}) => {
      this.orderForm.reset();
      console.log('data', data);
      this.router.navigate(['/']);
    });
    // console.warn('Your order has been submitted', this.orderForm.value);
    // this.orderService.addOrder(this.orderForm.value);
  }
  handleCancel() {
    this.orderForm.reset();
    this.router.navigate(['/']);
  }
}
