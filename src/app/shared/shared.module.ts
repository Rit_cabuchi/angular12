import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { ButtonComponentComponent } from './components/button-component/button-component.component';
import { CardComponentComponent } from './components/card-component/card-component.component';
import { AlertComponentComponent } from './components/alert-component/alert-component.component';

@NgModule({
  declarations: [
    ButtonComponentComponent,
    CardComponentComponent,
    AlertComponentComponent,
  ],
  imports: [CommonModule, RouterModule],
  exports: [ButtonComponentComponent, CardComponentComponent],
})
export class SharedModule {}
