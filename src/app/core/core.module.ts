import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { SharedModule } from '@shared/shared.module';
import { RouterModule } from '@angular/router';

/**Import Component  */
import { PageNotFoundComponent } from './components/page-not-found/page-not-found.component';
import { NavComponent } from './components/nav/nav.component';

@NgModule({
  declarations: [PageNotFoundComponent, NavComponent],
  imports: [CommonModule, HttpClientModule, SharedModule, RouterModule],
  exports: [NavComponent, PageNotFoundComponent],
})
export class CoreModule {}
