import { Component, Output, EventEmitter, Input } from '@angular/core';
@Component({
  selector: 'app-btn-component',
  templateUrl: './button-component.component.html',
  styleUrls: ['./button-component.component.scss'],
})
export class ButtonComponentComponent {
  @Output() clickBtn = new EventEmitter();
  @Input() routerLink!: string[];
  @Input() type: string = 'primary';
  constructor() {}
}
