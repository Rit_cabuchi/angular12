export const headerTableOrder = [
  'No',
  'Order ID',
  'Name',
  'Quality',
  'Price',
  'Created',
  '',
];

export interface Order {
  order_id: string;
  name: string;
  quality: number;
  price: number;
  created: string;
}

export interface Ordersm {
  name: string;
  price: number;
  quality: number;
  create_date: Date;
  _id: string;
}

export const orders = [
  {
    order_id: 'OR01',
    name: 'OrderONE',
    quality: 2,
    price: 2000,
    created: '12/12/2021',
  },
  {
    order_id: 'OR02',
    name: 'OrderONE',
    quality: 3,
    price: 2000,
    created: '12/12/2021',
  },
  {
    order_id: 'OR03',
    name: 'OrderONE',
    quality: 4,
    price: 2000,
    created: '12/12/2021',
  },
  {
    order_id: 'OR04',
    name: 'OrderONE',
    quality: 5,
    price: 2000,
    created: '12/12/2021',
  },
];
